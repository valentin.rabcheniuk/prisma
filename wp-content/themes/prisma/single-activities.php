<?php get_header();?>
    <div class="main">
        <a href=".header" class="scroll-top"></a>

        <?php (new Breadcrumbs())->render();?>
        <?php (new Single_Activities_Menu())->render();?>
        <?php (new Single_Activities())->render();?>
        <?php (new General_Realization())->render();?>
        <?php (new General_News())->render();?>
    </div>

    <?php get_footer();?>