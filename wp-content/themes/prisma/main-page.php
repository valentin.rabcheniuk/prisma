<?php
/* Template Name: Главная страница */
?>

<?php get_header(); ?>
    
    <div class="main">
        <a href=".header" class="scroll-top"></a>
        <?php (new Main_Page_Slider())->render();?>
        <?php (new Main_Page_Intro())->render();?>
        <?php (new Main_Page_Activities())->render();?>
        <?php (new General_Realization())->render();?>
        <?php (new General_News())->render();?>
    </div>

   <?php get_footer(); ?>
  

   



