<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- build:css styles/plagins.css -->
    <link rel="stylesheet" href="<?=TEMPLATE_PATH?>styles/fancybox.css">
    <link rel="stylesheet" href="<?=TEMPLATE_PATH?>styles/formstyler.css">
    <link rel="stylesheet" href="<?=TEMPLATE_PATH?>styles/normalize.css">
    <link rel="stylesheet" href="<?=TEMPLATE_PATH?>styles/slick.css">
        <!-- add you plagin css here -->

    <!-- endbuild -->

    <!-- build:css styles/main.css -->
    <link rel="stylesheet" href="<?=TEMPLATE_PATH?>styles/dev_0_basic.css">
    <link rel="stylesheet" href="<?=TEMPLATE_PATH?>styles/dev_1.css">
        <!-- add you develop css here -->

    
<?php wp_head();?>
</head>
<body>

<div class="global-wrapper">
    
<?dynamic_sidebar('sidebar-1')?>
    
    
   
   
