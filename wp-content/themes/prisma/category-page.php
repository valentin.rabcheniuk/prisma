<?php /** Template Name:  Category */ ?>
<?php get_header();?>
<div class="main">
        <a href=".header" class="scroll-top"></a>
        <?php (new Breadcrumbs())->render();?>
        <?php (new Single_Realization_Menu())->render();?>
        <?php (new Category())->render();?>
        <?php (new General_News())->render();?>
</div>
<?php get_footer();?>