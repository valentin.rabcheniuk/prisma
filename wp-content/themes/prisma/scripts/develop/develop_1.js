'use strict';

$(document).ready(function () {

  $('.js-main-slider').slick({
    arrows: true,
    dots: true,
    infinite: true,
    speed: 500,
    fade: true,
    cssEase: 'linear',
    responsive: [{
      breakpoint: 992,
      settings: {
        arrows: false
      }
    }]
  });

  var btn = $('a[href=".header"]');

  $(window).scroll(function () {
    if ($(window).scrollTop() > 600) {
      btn.addClass('show');
    } else {
      btn.removeClass('show');
    }
  });

  btn.on('click', function (e) {
    e.preventDefault();
    $('html, body').animate({ scrollTop: 0 }, '300');
  });

  $('#btn').click(function () {
    $(this).toggleClass('active');
    $('#box').toggleClass('active');
  });

  $(window).keydown(function () {
    if ($('#box').hasClass('active') && event.keyCode === 27) {
      $('#btn').removeClass('active');
      $('#box').removeClass('active');
    }
  });

  $('.ham-dropbtn').click(function () {
    $(this).toggleClass('drop-active');
  });

  $('.tabs-stage .tabs-stage__block').css('display', 'none');
  $('.tabs-stage .tabs-stage__block:first').css('display', 'block');
  $('.tab-nav a:first').addClass('tab-active');

  $('.tab-nav a').on('click', function (event) {
   // event.preventDefault();
    $('.tab-nav a').removeClass('tab-active');
    $(this).addClass('tab-active');
    $('.tabs-stage .tabs-stage__block').css('display', 'none');
    $($(this).attr('href')).css('display', 'block');
  });

  $(document).on('click', '.myBtn', function () {
    var myTargetModal = '#' + $(this).data('bid');
    $('#myModal').hide();
    $('.modal-content').hide();
    $('#myModal').fadeIn();
    $(myTargetModal).fadeIn();
  });
  $("body").on("click", ".close", function () {
    $('#myModal').hide();
    $('.modal-content').hide();
  });
  $(document).click(function (e) {
    if ($(e.target).is('#myModal')) {
      $('#myModal').hide();
      $('.modal-content').hide();
    }
  });

  $('.block-ttl').each(function (index, value) {
    var name = $(this).text();
    if (name.length > 100) {
      var shortname = '<span class="block-ttl" >' + name.substring(0, 100) + " ..." + '</span>';
      $(this).replaceWith(shortname);
    }
  });
});

window.onscroll = function () {
  scrollFunction();
};

function scrollFunction() {

  if ($(window).width() > 991) {

    if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
      $('.header').css('box-shadow', '.2rem .2rem 2rem rgba(0, 0, 0, 0.3)');
      $('.h-menu').css('padding-top', '0rem');
      $('.h-menu').css('padding-bottom', '0rem');
    } else {
      $('.header').css('box-shadow', 'unset');
      $('.h-menu').css('padding-top', '1.5rem');
      $('.h-menu').css('padding-bottom', '1.5rem');
    }
  }
}

if ($(window).width() <= 991) {
  var checkOffset = function checkOffset() {
    if ($('.scroll-top').offset().top + $('.scroll-top').height() >= $('.footer').offset().top - 350) $('.scroll-top').css('position', 'absolute');
    if ($(document).scrollTop() + window.innerHeight < $('.footer').offset().top) $('.scroll-top').css('position', 'fixed'); // restore when you scroll up
  };

  $(document).scroll(function () {
    checkOffset();
  });
}

$(".js-main-slider").on("beforeChange", function (event, slick, currentSlide) {
  $('.slide-ttl').removeClass('fadeLeftIn');
  $('.slide-ttl').addClass('fadeLeftOut');

  $('.slide-subttl').removeClass('fadeLeftIn');
  $('.slide-subttl').addClass('fadeLeftOut');

  $('.slide-txt').removeClass('fadeLeftIn');
  $('.slide-txt').addClass('fadeLeftOut');
});

$(".js-main-slider").on("afterChange", function (event, slick, currentSlide, nextSlide) {
  $('.slide-ttl').removeClass('fadeLeftOut');
  $('.slide-ttl').addClass('fadeLeftIn');

  $('.slide-subttl').removeClass('fadeLeftOut');
  $('.slide-subttl').addClass('fadeLeftIn');

  $('.slide-txt').removeClass('fadeLeftOut');
  $('.slide-txt').addClass('fadeLeftIn');
});
//# sourceMappingURL=develop_1.js.map
