<?php
/* Template Name: Contact Page */
?>
<?php get_header();?>
<div class="main">
        <a href=".header" class="scroll-top"></a>

        <?php (new Breadcrumbs())->render();?>
        <?php (new Contact_Page_Map())->render();?>        
        <?php (new Contact_Page_Question())->render();?>
        <?php (new Contact_Page_Contact())->render();?>
        <?php (new Contact_Page_City())->render();?>
</div>
<?php get_footer();?>