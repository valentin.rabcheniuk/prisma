<?php get_header();?>
    <div class="main">
        <a href=".header" class="scroll-top"></a>

        <?php (new Breadcrumbs())->render();?>

        <?php (new Single_News())->render();?>
        
        <?php (new General_Realization())->render();?>
    </div>

    <?php get_footer();?>