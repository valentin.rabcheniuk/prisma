<?dynamic_sidebar('sidebar-2')?>
<!-- /FOOTER -->
</div>

<!-- SCRIPTS -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

<!-- build:js scripts/validate_script.js -->
<script type="text/javascript" src="<?=TEMPLATE_PATH?>scripts/validate_script.js" ></script>
<!-- endbuild -->

<!-- build:js scripts/plagins.js -->
<script type="text/javascript" src="<?=TEMPLATE_PATH?>scripts/plagins/device.js" ></script>
<script type="text/javascript" src="<?=TEMPLATE_PATH?>scripts/plagins/jquery.fancybox.min.js" ></script>
<script type="text/javascript" src="<?=TEMPLATE_PATH?>scripts/plagins/jquery.formstyler.min.js" ></script>
<script type="text/javascript" src="<?=TEMPLATE_PATH?>scripts/plagins/jquery.validate.min.js" ></script>
<script type="text/javascript" src="<?=TEMPLATE_PATH?>scripts/plagins/maskInput.js" ></script>
<script type="text/javascript" src="<?=TEMPLATE_PATH?>scripts/plagins/slick.js" ></script>
    <!-- add you plagins js here -->

<!-- endbuild -->

<!-- build:js scripts/main.js -->
<script type="text/javascript" src="<?=TEMPLATE_PATH?>scripts/basic_scripts.js" ></script>
<script type="text/javascript" src="<?=TEMPLATE_PATH?>scripts/develop/develop_1.js" ></script>
    <!-- add you develop js here -->
<!-- endbuild -->

<!-- /SCRIPTS -->
<?php wp_footer(); ?>
</body>

</html>

