<?php 
class Widget_Footer extends WP_Widget {

        function __construct() {
        // Запускаем родительский класс
        parent::__construct(
            'footer', // ID виджета, если не указать (оставить ''), то ID будет равен названию класса в нижнем регистре: my_widget
            'Footer',
            array('description' => 'Widget Footer')
        );

        // стили скрипты виджета, только если он активен
        if ( is_active_widget( false, false, $this->id_base ) || is_customize_preview() ) {
            add_action('wp_enqueue_scripts', array( $this, 'add_my_widget_scripts' ));
            add_action('wp_head', array( $this, 'add_my_widget_style' ) );
        }
    }

    // Вывод виджета
    function widget( $args, $instance ){
            $this->copyright=get_field("copyright",'widget_'.$args['widget_id']);
            $this->info=get_field("info",'widget_'.$args['widget_id']);
            $this->phones_first=get_field("phones_first",'widget_'.$args['widget_id']);
            $this->phones_second=get_field("phones_second",'widget_'.$args['widget_id']);
            $this->email=get_field("email",'widget_'.$args['widget_id']);
            $this->address=get_field("address",'widget_'.$args['widget_id']);
            $this->button_name=get_field("button_name",'widget_'.$args['widget_id']);

        ?>
        <div class="modal" id="myModal">
            <div class="modal-content question-content" id="modal-question">
                <span class="close"></span>
                
                <span class="question-ttl">Задать вопрос</span>
                <span class="question-txt">Спасибо! Мы обязательно свяжемся с Вами и ответям на все Ваши вопросы</span>
                <form class="question-form" action="<?=admin_url('admin-ajax.php')?>">
                <input type = "hidden" name = "action" value = "prisma_question">
                    <div class="contact-form-row cfix">
                        <div class="contact-form-item">
                            <div class="contact-form-item-input form_row">
                                <div class="form_input question-inp">
                                    <input class="inp" type="text" required name="questionFormName" placeholder="Ваше имя*">
                                </div>
                            </div>
                        </div>
                        <div class="contact-form-item">
                            <div class="contact-form-item-input form_row">
                                <div class="form_input question-inp">
                                    <input class="inp" type="tel" required name="questionFormPhone" placeholder="Номер телефона*">
                                </div>
                            </div>
                        </div>
                        <div class="contact-form-item">
                            <div class="contact-form-item-input form_row">
                                <div class="form_input question-inp">
                                    <input class="inp" type="email" required name="questionFormEmail" placeholder="Email*">
                                </div>
                            </div>
                        </div>
                        <textarea class="txt-area qusetion-txtarea" name="questionFormMessage" placeholder="Сообщение"></textarea>
                        <input class="form-btn question-btn" type="submit" value="Отправить">
                    </div>
                </form>
            </div>
        
            <div class="modal-content success-content" id="modal-success">
                <span class="close"></span>
                
                <span class="success-ttl">Спасибо!</span>
                <span class="success-txt">Ваша заявка принята в обработку</span>
                <span class="success-icon"></span>
            </div>
        </div>
    <!-- FOOTER -->
    <footer class="footer">
            <div class="cg f-menu">
                <div class="f-menu__info">
                    <span class="info-copy"> <?=$this->copyright ?> </span>
                    <span class="info-protect"> <?=$this->info ?> </span>
                </div>
                <div class="question-wrap">
                    <a href="javascript:void(0);" data-bid="modal-question" class="f-menu__question myBtn"><?=$this->button_name?></a>
                </div>
                <div class="f-menu__contacts">
                    <a href="<?=$this->address["address_url"] ?>" target="_blank" class="contact-link contact-address"> <?=$this->address["address"]?> </a>
                    <?php $this->phones_first($this->phones_first);?>
                    <?php $this->phones_second($this->phones_second);?>
                    <a href="mailto:<?=$this->email?>" class="contact-link contact-mail"> <?=$this->email?> </a>
                </div>
                <a href="https://mgn.com.ua/" target="_blank" class="f-menu__design">
                    <span class="design-txt">дизайн сайта</span>
                    <img src="<?php echo get_template_directory_uri(); ?>/img/design-img.png" alt="Dising logotype" class="design-img">
                </a>
            </div>
    </footer>
    
    <?php
    }

    // Сохранение настроек виджета (очистка)
    function update( $new_instance, $old_instance ) {
    }

    // html форма настроек виджета в Админ-панели
    function form( $instance ) {
    }

    // скрипт виджета
    function add_my_widget_scripts() {
        // фильтр чтобы можно было отключить скрипты
        if( ! apply_filters( 'show_my_widget_script', true, $this->id_base ) )
            return;

        $theme_url = get_stylesheet_directory_uri();

        wp_enqueue_script('my_widget_script', $theme_url .'/my_widget_script.js' );
    }

    // стили виджета
    function add_my_widget_style() {
        // фильтр чтобы можно было отключить стили
        if( ! apply_filters( 'show_my_widget_style', true, $this->id_base ) )
            return;
        ?>
        <style>
            .my_widget a{ display:inline; }
        </style>
        <?php
    }

    function phones_first($phones_first){
        if($phones_first)
        {   $phones=explode("\n",$phones_first);
            ?><div class="contact-phones__first"><?
            foreach($phones as $phone)
            {       
                    ?><a href="tel:+<?=preg_replace('/[^\d]/', '', $phone);?>" class="contact-link"><?=$phone?></a><?
            }
        ?></div><?
        
    }
    }

    function phones_second($phones_second){
        if($phones_second)
        {   $phones=explode("\n",$phones_second);
            ?><div class="contact-phones__second"><?
            foreach($phones as $phone)
            {       
                    ?><a href="tel:+<?=preg_replace('/[^\d]/', '', $phone);?>" class="contact-link"><?=$phone?></a><?
            }
        ?></div><?
        
    }
    }
}
