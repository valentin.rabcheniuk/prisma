<?php
/* Template Name: Articles Page */
?>
<?php get_header();?>
    <div class="main">
        <a href=".header" class="scroll-top"></a>

        
        <?php (new Breadcrumbs())->render();?>
        <?php (new Archive_Articles())->render();?>
        <?php (new General_News())->render();?>
        <?php (new General_Realization())->render();?>
    </div>

<?php get_footer();?>