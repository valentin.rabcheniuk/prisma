<?php /** Template Name:  O Nas */ ?>
<?php get_header();?>
<div class="main">
        <a href=".header" class="scroll-top"></a>
        <?php (new Breadcrumbs())->render();?>
        <?php (new O_Nas_Menu())->render();?>
        <?php (new General_Realization())->render();?>
        <?php (new General_News())->render();?>
</div>
<?php get_footer();?>