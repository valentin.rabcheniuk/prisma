<?php

define('TEMPLATE_PATH', get_template_directory_uri() . '/');

function create_post_type(){
    register_post_type('news',
    array(
        'labels'=>array(
            'name'=>('News'),
            'singular_name'=>('News')
        ),
        'public'=>true,
        'taxonomies' => array( 'category' ),
        'has_archive'=>true,
        'supports'=>array('title','editor','thumbnail','post-formats','excerpt' ,'post-thumbnails')
    )
    );

    register_post_type('realization',
    array(
        'labels'=>array(
            'name'=>__('Realization'),
            'singular_name'=>__('Realization')
        ),
        'public'=>true,
        'taxonomies' => array( 'category' ),
        'has_archive'=>true,
        'supports'=>array('title','editor','thumbnail','post-formats','excerpt' ,'post-thumbnails')
    )
    );

    register_post_type('activities',
    array(
        'labels'=>array(
            'name'=>__('Activities'),
            'singular_name'=>__('Activities')
        ),
        'public'=>true,
        'taxonomies' => array( 'category' ),
        'has_archive'=>true,
        'supports'=>array('title','editor','thumbnail','post-formats','excerpt' ,'post-thumbnails')
    )
    );

    register_post_type('message',
    array(
        'labels'=>array(
            'name'=>__('Message'),
            'singular_name'=>__('Message')
        ),
        'public'=>true,
        'taxonomies' => array( 'category' ),
        'has_archive'=>true,
        'supports'=>array('title','editor','thumbnail','post-formats','excerpt' ,'post-thumbnails')
    )
    );

    register_post_type('technology',
    array(
        'labels'=>array(
            'name'=>__('Technology'),
            'singular_name'=>__('Technology')
        ),
        'public'=>true,
        'taxonomies' => array( 'category' ),
        'has_archive'=>true,
        'supports'=>array('title','editor','thumbnail','post-formats','excerpt' ,'post-thumbnails')
    )
    );

    
}    
add_action('init', 'create_post_type');

function prisma_setup() {
    load_theme_textdomain('prisma');

    add_theme_support('title-tag');

    add_theme_support('custom-logo', array('height' => '90', 'width' => '130', 'flex-height' => true));

    add_theme_support('post-thumbnails');
    set_post_thumbnail_size(270,150);

    add_theme_support('html5', array('search_form', 'comment-form', 'comment-list', 'gallery', 'caption'));

    add_theme_support('post-formats', array('aside', 'image', 'video', 'gallery'));

    register_nav_menu('primary', 'Primary menu');
    add_theme_support('widgets');

    
}
add_action('after_setup_theme', 'prisma_setup');



function prisma_pagination( $args = array() ) {
    
    $defaults = array(
        'range'           => 4,
        'custom_query'    => FALSE,
        'previous_string' => __( 'Previous', 'text-domain' ),
        'next_string'     => __( 'Next', 'text-domain' ),
        'before_output'   => '<div class="project-pagination pagination"><ul class="pagination-link">',
        'after_output'    => '</ul></div>'
    );
    
    $args = wp_parse_args( 
        $args, 
        apply_filters( 'wp_bootstrap_pagination_defaults', $defaults )
    );
    
    $args['range'] = (int) $args['range'] - 1;
    if ( !$args['custom_query'] )
        $args['custom_query'] = @$GLOBALS['wp_query'];
    $count = (int) $args['custom_query']->max_num_pages;
    $page  = intval( get_query_var( 'paged' ) );
    $ceil  = ceil( $args['range'] / 2 );
    
    if ( $count <= 1 )
        return FALSE;
    
    if ( !$page )
        $page = 1;
    
    if ( $count > $args['range'] ) {
        if ( $page <= $args['range'] ) {
            $min = 1;
            $max = $args['range'] + 1;
        } elseif ( $page >= ($count - $ceil) ) {
            $min = $count - $args['range'];
            $max = $count;
        } elseif ( $page >= $args['range'] && $page < ($count - $ceil) ) {
            $min = $page - $ceil;
            $max = $page + $ceil;
        }
    } else {
        $min = 1;
        $max = $count;
    }
    
    $echo = '';
    $previous = intval($page) - 1;
    $previous = esc_attr( get_pagenum_link($previous) );
    
    $firstpage = esc_attr( get_pagenum_link(1) );
    if ( $firstpage && (1 != $page) )
        $echo .= '<li class="previous"><a href="' . $firstpage . '" class="pagination-link">' . __( 'First', 'text-domain' ) . '</a></li>';

    if ( $previous && (1 != $page) )
        $echo .= '<li><a href="' . $previous . '" class="pagination-link" title="' . __( 'previous', 'text-domain') . '">' . $args['previous_string'] . '</a></li>';
    
    if ( !empty($min) && !empty($max) ) {
        for( $i = $min; $i <= $max; $i++ ) {
            if ($page == $i) {
                $echo .= '<li class="active"><span class="pagination-arr">' . str_pad( (int)$i, 1, '0', STR_PAD_LEFT ) . '</span></li>';
            } else {
                $echo .= sprintf( '<li><a href="%s" class="pagination-link">%2d</a></li>', esc_attr( get_pagenum_link($i) ), $i );
            }
        }
    }
    
    $next = intval($page) + 1;
    $next = esc_attr( get_pagenum_link($next) );
    if ($next && ($count != $page) )
        $echo .= '<li><a href="' . $next . '" class="pagination-link" title="' . __( 'next', 'text-domain') . '">' . $args['next_string'] . '</a></li>';
    
    $lastpage = esc_attr( get_pagenum_link($count) );
    if ( $lastpage ) {
        $echo .= '<li class="next"><a href="' . $lastpage . '" class="pagination-link">' . __( 'Last', 'text-domain' ) . '</a></li>';
    }

    if ( isset($echo) )
        echo $args['before_output'] . $echo . $args['after_output'];
}

function prisma_register_widgets() {
    register_widget( 'Widget_Header' );
    register_widget( 'Widget_Footer' );
   
}
add_action( 'widgets_init', 'prisma_register_widgets' );
register_sidebar('sidebar','header');
register_sidebar('Footer','footer');

function prisma_question(){
    $recepient = "valentin.rabchenuk@gmail.com";
    $sitename = "Prisma";

    $name = trim($_POST["questionFormName"]);
    $phone = trim($_POST["questionFormPhone"]);
    $email = trim($_POST["questionFormEmail"]);
    $text = trim($_POST["questionFormMessage"]);
    $message = "Имя: $name \nТелефон: $phone \nEmail: $email \nСообщение: $text";

    $pagetitle = "Новая заявка с сайта \"$sitename\"";
    mail($recepient, $pagetitle, $message);

    $post = array(
        'post_title'    => $sitename,
        'post_content'  => $message,
        'post_status'   => 'publish',
        'post_type'     =>'message'
    );

    
    $post_id = wp_insert_post( $post );

    echo 'true';
    die;
}
add_action("wp_ajax_prisma_question","prisma_question");
add_action("wp_ajax_nopriv_prisma_question","prisma_question");


function wpseo_change_breadcrumb_link($link_output, $link)
{
    $text_to_change = 'Realization';
    if ($link['text'] == $text_to_change) {
        $link_output = 'Все проекты';
    }
    $text_to_change = 'Technology';
    if ($link['text'] == $text_to_change) {
        $link_output = '';
    }
    $text_to_change = 'Activities';
    if ($link['text'] == $text_to_change) {
        $link_output = '';
    }
    return $link_output;

}
add_filter('wpseo_breadcrumb_single_link', 'wpseo_change_breadcrumb_link', 10, 2);


class Theme_AutoLoader
{
    public static function init()
    {

        self::load(__DIR__ . '/template-parts');
        self::load(__DIR__ . '/includes');
    }

    public static function load($path)
    {
        $path = $path . '/';
        $path = preg_replace('/\/+$/', '/', $path);

        $files = array_diff(scandir($path), ['.', '..']);

        if (!$files) {
            return;
        }

        foreach ($files as $file) {
            if (is_dir($path . $file)) {
                self::load($path . $file);
            } elseif (preg_match('/(\.php)$/', $file)) {
                require_once $path . $file;
            }
        }
    }
}

Theme_AutoLoader::init();







