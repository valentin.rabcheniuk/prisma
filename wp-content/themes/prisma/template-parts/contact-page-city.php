<?php
class Contact_Page_City{
    public function __construct(){
        $this->cities= get_field("cities");
    }
    public function render(){?>
       <section class="cg contact-members">
       <?foreach($this->cities as $city){?>
            <div class="contact-members__block">
                <span class="member-city"><?=$city["city"]?></span>
                <div class="member-person">
                    <span class="member-person__name"><?=$city["worker"]?></span>
                    <span class="member-person__position"><?=$city["position"]?></span>
                </div>
                <a href="tel:+<?=preg_replace('/[^\d]/', '', $city["phone"]);?>" class="member-phone"><?=$city["phone"];?></a>
                <a href="mailto:<?=$city["email"]?>" class="member-mail"><?=$city["email"]?></a>
                <a href="<?=$city["map"]?>" class="member-location"><?=$city["address"]?></a>
            </div>
       <?}?>
        </section>
    <?}
}