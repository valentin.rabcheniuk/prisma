<?php
class Main_Page_Activities{

public function __construct(){
    
}

public function render(){
    ?>
    <section class="cg main-pass">
            <div class="link-wrap">
                <h1 class="main-implement__link main-pass__ttl">Направления деятельности</h1>
            </div>
            
            <div class="main-pass__blocks">
            <?php $args=array('post_type'=>'activities', 'post_per_page'=>9);?>
            <?php $loop=new WP_Query( $args );?>
            <?php while ($loop->have_posts() ) : $loop->the_post(); ?>

                <div class="main-pass__block">
                    <div class="pass-left">
                        <div class="pass-left__wrap">
                            <img src="<?php the_post_thumbnail_url(); ?>" alt="Pass image" class="pass-img">
                        </div>                  
                    </div>
                    <div class="pass-right">
                        <span class="pass-right__ttl"><?php the_title(); ?></span>
                        <span class="pass-right__txt"><?php the_content(); ?></span>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </section>
<?
}
}




