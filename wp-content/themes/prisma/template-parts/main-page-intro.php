<?php
class Main_Page_Intro{

    public function __construct(){
        $this->title=get_field("title");
        $this->content=get_field("content");
        $this->image1=get_field("image1");
        $this->image2=get_field("image2");
    }

    public function render(){
        ?>
        <section class="cg main-intro">
        <h2 class="main-intro__ttl"><?=$this->title?></h2>
        <div class="main-intro__info">
            <div class="intro-info__wrap">
                <img src="<?=$this->image1;?>" alt="" class="img">
            </div>
            <div class="intro-info__wrap">
                <img src="<?=$this->image2;?>" alt="" class="img">
            </div>
            <p class="intro-info__txt"><?=str_replace(["[","]"],['<span class="green-txt">','</span>'],$this->content);?> </p>
        </div>
    </section>
    <?
    }
}


