<?php

class Breadcrumbs {
    public function __construct( ) {}

    public function render() {?>
        <div class="cg breadcrumbs"><?
        echo $this->get_render();
        ?></div><?
    }

    public function get_render() {
        if ( function_exists('yoast_breadcrumb') ) {
        
            yoast_breadcrumb('<p id="breadcrumbs" class="breadcrumbs-link">','</p>');
            
        }
        
    }
}
