<?php
class Contact_Page_Question{
    public function __construct(){}
    public function render(){?>
        <section class="cg contact-info">
            <div class="contact-info__request">
                <span class="info-request__ttl">Задать вопрос</span>
                <form action="<?=admin_url('admin-ajax.php')?>" class="info-request__form">
                <input type="hidden" name="action" value="prisma_question">
                    <div class="contact-form-item">
                        <div class="contact-form-item-input form_row">
                            <div class="form_input question-inp">
                                <input type="text" class="inp" required name="contactFormName" placeholder="Ваше имя*">
                            </div>
                        </div>
                    </div>
                    <div class="contact-form-item">
                        <div class="contact-form-item-input form_row">
                            <div class="form_input question-inp">
                                <input type="email" class="inp" required name="contactFormEmail" placeholder="Email*">
                            </div>
                        </div>
                    </div>
                    <div class="contact-form-item">
                        <div class="contact-form-item-input form_row">
                            <div class="form_input question-inp">
                                <input type="tel" class="inp" required name="contactFormPhone" placeholder="Номер телефона*">
                            </div>
                        </div>
                     </div>
                    <input type="text" class="inp" name="contactFormTheme" placeholder="Тема письма">
                    <textarea class="txt-area contact-txtarea" name="contactFormMessage" placeholder="Сообщение"></textarea>
                    <input type="submit" class="form-btn" value="Отправить">
                </form>
            </div>
    <?}
}