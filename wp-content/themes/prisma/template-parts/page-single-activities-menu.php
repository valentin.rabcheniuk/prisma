<?php
class Single_Activities_Menu{
    public function __Construct(){
        $this->widget=wp_get_sidebars_widgets();
        $widget_id=array_values($this->widget["sidebar-1"])[0];
        $this->menu=get_field("menu",'widget_'.$widget_id);
    }
    public function render(){?>
        <section class="cg bc about-content">
        <div class="tab-nav about-content__nav"><?
        foreach ($this->menu as $item) {
            if($item["title"]=="Направления"){
                foreach($item["submenus"] as $link){
                    if(stristr($link["link"]["url"],$_SERVER['REQUEST_URI'])){ ?>   
                     <a class="tab-nav__link about-nav__link tab-active" href="<?=$link["link"]["url"]?>"><?=$link["link"]["title"]?></a>
            
               <? }else{?>
                <a class="tab-nav__link about-nav__link" href="<?=$link["link"]["url"]?>"><?=$link["link"]["title"]?></a>
               <?}
               }
            }
        } ?></div><?
    }
}