<?php
class Contact_Page_Contact{
    public function __construct(){
        $this->contact= get_field("contact");
    }
    public function render(){?>
       <div class="contact-info__block">
                <span class="info-ttl">Контакты</span>
                <span class="info-city"><?=$this->contact["city"]?></span>
                <div class="info-datas">
                    <?=$this->phones_first($this->contact["phones_1"]);?> 
                    <?=$this->phones_second($this->contact["phones_2"]);?>
                    <a href="mailto:<?=$this->contact["email"]?>" class="info-datas__mail"><?=$this->contact["email"]?></a>
                </div>
                <div class="info-address">
                    <span class="info-address__txt"><?=$this->contact["address"]?></span>
                    <span class="info-address__txt">Офис: <?=$this->contact["office"]?></span>
                    <span class="info-address__txt">Производство: <?=$this->contact["production"]?></span>
                </div>
            </div>
        </section>
    <?}

    function phones_first($phones_first){
        if($phones_first)
        {   $phones=explode("\n",$phones_first);
            ?><div class="info-datas__phone1"><?
            foreach($phones as $phone)
            {       
                ?><a class="phone-link" href="tel:+<?=preg_replace('/[^\d]/', '', $phone);?>" class="contact-link"><?=$phone?></a><?
            }
        ?></div><?
        
    }
    }

    function phones_second($phones_second){
        if($phones_second)
        {   $phones=explode("\n",$phones_second);
            ?><div class="info-datas__phone1"><?
            foreach($phones as $phone)
            {       
                ?><a class="phone-link" href="tel:+<?=preg_replace('/[^\d]/', '', $phone);?>" class="contact-link"><?=$phone?></a><?
            }
        ?></div><?
        
    }
    }
}