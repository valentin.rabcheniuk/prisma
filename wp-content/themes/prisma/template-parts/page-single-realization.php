<?

class Single_Realization{
    public function __construct(){}
    public function render(){?>
       <section class="cg implementation">
            <div class="bc-open">
                <div><?php the_title()?></div>
                <div><?php the_content()?></div>
                <img src="<?php the_post_thumbnail_url();?>">
                <div><?php the_time()?></div>
            </div>
        </section>
 <?}
}