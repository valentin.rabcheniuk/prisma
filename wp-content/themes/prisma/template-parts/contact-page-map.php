<?php
class Contact_Page_Map{
    public function __construct(){
        $this->map= get_field("map");
    }
    public function render(){?>
        <section class="contact-map__wrap">
                
                <?=$this->map;?>
        </section>
    <?}
}