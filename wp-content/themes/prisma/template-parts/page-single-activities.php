<?

class Single_Activities{
    public function __construct(){}
    public function render(){?>
       <section class="tabs-stage about-stage">
            <div class="tabs-stage__block">
                <div><?php the_title()?></div>
                <div><?php the_content()?></div>
                <img src="<?php the_post_thumbnail_url();?>">
                <div><?php the_time()?></div>
            </div>
        </section>
 <?}
}