<?php
class Main_Page_Slider{

    public function __construct(){
        $this->slider=get_field("slider");
    }

    public function render(){
        ?>
        <section class="main-slider__block">
            <div class="main-slider js-main-slider">
        <?php        
        
        foreach ($this->slider as $slide){
            ?>
                <div>
                    <a href="<?=$slide["url"]["url"]?>">
                        <div class="main-slider__wrap">
                            <img src="<?=$slide["image"]?>" alt="Slide image" class="main-slider__img">
                            <div class="main-slider__info">    
                                <span class="fadeLeftIn slide-ttl"><?=str_replace(["[","]"],['<span class="green-txt">','</span>'],$slide["title"]);?></span>                               
                                <span class="fadeLeftIn d-9 slide-txt"><?=$slide["content"]?></span>
                            </div>
                        </div>
                    </a>
                </div><?php
        }
           ?> </div>
        </section>
    <?
    }
}
