<?php
class General_News{

    public function __construct(){
    }

    public function render(){
        ?>
        <section class="cg mb-distance-2">
            <div class="link-wrap">
                <a href="#" class="main-implement__link">Новости</a>
            </div>
            <div class="main-implement__blocks">
            <?php $args=array('post_type'=>'News', 'posts_per_page'=>8, 'category_name' => $this->categories[0]->name);?>
            <?php $loop=new WP_Query( $args );?>
            <?php while ($loop->have_posts() ) : $loop->the_post(); ?>
                <div class="main-implement__block">
                    <a href="<?php the_permalink()?>" class="block-link">
                        <div class="block-img__wrap">
                            <img src="<?php the_post_thumbnail_url(); ?>" alt="Block image" class="block-img">
                            <img src="<?=TEMPLATE_PATH?>img/icon/implement-icon__video.svg" alt="Post icon" class="implement-post__icon video-icon">
                        </div>
                        <span class="block-date"><?php the_time('j F Y');?></span>
                        <span class="block-ttl"><?php the_title(); ?></span>
                        <?php $this->categories(get_the_ID());?>
                        
                        
                    </a>
                </div>
            <?php endwhile;?>   
            </div>
        </section><?php
    }

    private function categories($id){
        $categories_data=get_the_category( $id );
         foreach($categories_data as $category){
            $name_category[]=$category->name;}
           
            $categories=implode(',',$name_category);
            
        ?>
        <span class="block-tag">
       <?=$categories;?>
        </span><?
    }
}
