<?

class Archive_Realization{
    public function __construct(){}
    public function render()
    {?>
        <section class="cg news-block__wrap">
            <div class="news-block">
                <div class="project-ttl__wrap">
                    <a href="#" class="project-ttl">Реализации</a>
                </div>
                <div class="project-blocks">
                    <?php $args = array_merge( array('post_type' => 'Realization'));?>
                    <?php $loop = new WP_Query($args);?>
                    <?php while ($loop->have_posts()): $loop->the_post();
                    $this->project_block();
                    endwhile;?>
                </div>
            </div>
        </section>
    <?}
    private function categories($id)
    {
        $categories_data = get_the_category($id);
        foreach ($categories_data as $category) {
            $name_category[] = $category->name;}

        $categories = implode(',', $name_category);

        ?>
         <span class="block-tag">
        <?=$categories;?>
         </span><?

    }

    public function project_block()
    {?>
      <div class="project-blocks__block">
    
    <a href="<?php the_permalink()?>" class="block-link">
        <div class="block-img__wrap">
            <img src="<?php the_post_thumbnail_url();?>" alt="Block image" class="block-img">
            <img src="<?=TEMPLATE_PATH?>img/icon/implement-icon__video.svg" alt="Post icon" class="implement-post__icon video-icon">
        </div>
        <span class="block-date"><?php the_time('j F Y');?></span>
        <span class="block-ttl"><?php the_content();?></span>
        <?php $this->categories(get_the_ID());?>
    </a>
    </div>
    <?}
}     